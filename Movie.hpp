#ifndef __MOVIES_H
#define __MOVIES_H

#include <string>

class Movie {
	
	public:
		Movie(std::string path);
		std::string get_name() {return this->m_name;}
		std::string get_path() {return this->m_path;}

	private:
		std::string m_path;
		std::string m_name;
};

#endif

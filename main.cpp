#include "Server.hpp"
#include "Movie.hpp"
#include "Serie.hpp"
#include <vector>
#include <unistd.h>
#include "vlc.h"

int main(int argc, char *argv[]) {

	(void)argc;
	(void)argv;

	vector<Movie> movies;
	vector<Serie> series;
	Vlc vlc(movies, series);
	Server server(vlc, movies, series);
	server.start(8888);

	while (1) {

		sleep(1);

		if (vlc.get_state() == VLC_STATE_FINISHED && vlc.get_serie_nb() != -1) {

			cout << "trying to next" << endl;
		
			if (vlc.get_episode_nb() < series.at(vlc.get_serie_nb()).get_season(vlc.get_season_nb())->get_nb_episodes() -1) {
				cout << "next episode" << endl;
				vlc.play(-1, vlc.get_serie_nb(), vlc.get_season_nb(), vlc.get_episode_nb()+1, 0, 0);
			}
			else if (vlc.get_season_nb() < series.at(vlc.get_serie_nb()).get_nb_seasons() -1) {
				cout << "next season" << endl;
				vlc.play(-1, vlc.get_serie_nb(), vlc.get_season_nb()+1, 0, 0, 0);
			}
			else {
				cout << "End of seasons : stop" << endl;
				vlc.stop_cmd();
			}
		}
/*	
		cout << "nb de films : " << movies.size() << endl;
		cout << "nb de series : " << series.size() << endl;
		if (series.size() > 0) {
			cout << "nb de season : " << series.at(0).get_nb_seasons() << endl;
			cout << "nb de episodes S2 : " << series.at(0).get_nb_episodes(0) << endl;
			cout << "nom saison 1 : " << series.at(0).get_name_season(0) << endl;
		}
*/
	}
	return 0;
}

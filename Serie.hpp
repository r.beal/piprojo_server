#ifndef __SERIE_H
#define __SERIE_H

#include <string>
#include <list>
#include "Movie.hpp"
#include <iostream>
#include "Movie.hpp"
#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

using namespace std;

class Season {

	public:
		Season(std::string path);
		int get_nb_episodes() { return this->nb_episodes; }
		string get_name() { return this->m_name; }
		string get_episode_name(int i);
		Movie* get_episode(int episode);

	private:
		int nb_episodes;
		vector<Movie> movies;
		string m_name;
};

class Serie {
	
	public:
		Serie(std::string path);
		std::string get_name() {return this->m_name;}
		void scan();

		int get_nb_seasons() { return this->nb_seasons; }
		int set_name(string name) { this->m_name = name; }
		int get_nb_episodes(int season);
		Season* get_season(int season);
		string get_name_season(int season);
		string export_all_to_json();

	private:
		std::string m_path;
		std::string m_name;
		int nb_seasons;
		vector<Season> seasons;
};


#endif

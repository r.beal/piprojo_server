#ifndef __VLC_H
#define __VLC_H

#include <string>
#include <time.h>
#include "Movie.hpp"
#include "Serie.hpp"

#define VLC_STATE_STOPPED	0
#define VLC_STATE_PLAY		1
#define VLC_STATE_FINISHED	2
#define VLC_STATE_LAUNCHING	3

using namespace std;

class Vlc {
	
	public:
		Vlc(vector<Movie>& movies, vector<Serie>& series);
		~Vlc();

		void set_volume(unsigned int new_volume);
		void seek_cmd(int seek);
		void play(int movie, int serie, int season, int episode, unsigned int subtrack, unsigned int audiotrack);
		void play_cmd();
		void pause_cmd();
		void stop_cmd();
		void next_cmd();
		int get_state();
		void play(string path, unsigned int subtrack, unsigned int audiotrack);

		int get_movie_nb()   { return this->m_movie_nb;  }
		int get_serie_nb()   { return this->m_serie_nb;  }
		int get_season_nb()  { return this->m_season_nb; }
		int get_episode_nb() { return this->m_episode_nb; }

	private:
		int m_state;
		int m_current_volume;
		int m_current_time;
		unsigned int m_subtrack;
		unsigned int m_audiotrack;
		string m_path;
		int m_movie_nb;
		int m_serie_nb;
		int m_season_nb;
		int m_episode_nb;
		vector<Movie>* m_movies;
		vector<Serie>* m_series;
		
		time_t m_start_time;
};

#endif

#include <iostream>
#include "Movie.hpp"
#include "Serie.hpp"
#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

using namespace std; 

void scan_for_movies(std::string dir, vector<Movie>* movies);
void scan_for_series(std::string dir, vector<Serie>* series);
//void scan_for_series(char *dir, int depth, args_thread_t *args_thread);

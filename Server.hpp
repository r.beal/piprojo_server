#ifndef __SERVER_H
#define __SERVER_H

#include "Movie.hpp"
#include "Serie.hpp"
#include <pthread.h>
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <vector>
#include <unistd.h>
#include "scan.h"
#include "vlc.h"

using namespace std;

class Server {

	public:
		Server(Vlc& vlc, vector<Movie>& movies, vector<Serie>& series);
		void start(int port);
		int get_port() { return this->m_port; }
		int m_server_state;
		void scan();

		int get_nb_movies() { return this->m_movies->size(); }
		int get_nb_series() { return this->m_series->size(); }
		string get_movie_name(int index) { return this->m_movies->at(index).get_name(); }
		string get_serie_name(int index) { return this->m_series->at(index).get_name(); }
		string get_serie_json(int index) { return this->m_series->at(index).export_all_to_json(); }
		Vlc* m_vlc;

	private:
		pthread_t server_th;

		int m_port;
		vector<Movie>* m_movies;
		vector<Serie>* m_series;
		
		// server vars
};
		
static void *thread_server(void *arg);

#endif

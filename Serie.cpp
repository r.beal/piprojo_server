#include "Serie.hpp"

using namespace std;

Serie::Serie(std::string path) {
	this->m_path = path;

	this->scan();
}

void Serie::scan() {

	struct dirent *entry;
	struct stat statbuf;

	struct dirent **namelist;
	int n;
	int i=0;
	n = scandir(this->m_path.c_str(), &namelist,NULL,alphasort);
	if (n < 0)
		perror("scandir");
	else {
		while (i<n) {
			chdir(this->m_path.c_str());
			lstat(namelist[i]->d_name, &statbuf);
			if(strcmp(".", namelist[i]->d_name) == 0 ||
					strcmp("..",namelist[i]->d_name) == 0 || 
					! S_ISDIR(statbuf.st_mode) > 0) {
				i++;
				continue;
			}
			char tmp[100];
			snprintf(tmp, 100, "%s", namelist[i]->d_name);
			this->seasons.push_back(Season(tmp));
			chdir("..");
			free(namelist[i]);
			++i;
		}
		free(namelist);
	}

	this->nb_seasons = this->seasons.size();
}

int Serie::get_nb_episodes(int season) {

	if (season > this->seasons.size()) {
		return 0;
	}

	return this->seasons.at(season).get_nb_episodes();
}

string Serie::get_name_season(int season) {

	if (season > this->seasons.size()) {
		return 0;
	}

	return this->seasons.at(season).get_name();
}

Season *Serie::get_season(int season) {

	if (season < this->seasons.size()) {
		return &this->seasons.at(season);
	}
	else
		return nullptr;
}

Movie *Season::get_episode(int episode) {

	if (episode < this->movies.size()) {
		return &this->movies.at(episode);
	}
	else
		return nullptr;
}

string Season::get_episode_name(int episode) {

	if (episode > this->movies.size()) {
		return 0;
	}

	return this->movies.at(episode).get_name();
}

string Serie::export_all_to_json() {

	cout << "enter on eport to json " << endl;
	
	int i, j;
	string json;
	char tmp[100];

	json.append("{");

		
	for (i=0; i<this->nb_seasons ;i++) {
		cout << "nb saison : " << this->nb_seasons << ", nb_episodes : " << this->seasons.at(i).get_nb_episodes() << endl;
		if (i == 0)
			snprintf(tmp, 100, "\"%s\": [", this->get_name_season(i).c_str());
		else
			snprintf(tmp, 100, ",\"%s\": [", this->get_name_season(i).c_str());

		json.append(tmp);

		for (j=0; j<this->seasons.at(i).get_nb_episodes(); j++) {
			if (j==0)	
				snprintf(tmp, 100, "\"%s\"", this->seasons.at(i).get_episode_name(j).c_str());
			else
				snprintf(tmp, 100, ",\"%s\"", this->seasons.at(i).get_episode_name(j).c_str());
			json.append(tmp);
		}
		json.append("]");
	}
	json.append("}");

	cout << "oh putain : " << json << endl;

	return json;

}

Season::Season(std::string path) {

	struct dirent *entry;
	struct stat statbuf;

	struct dirent **namelist;
	int n;
	int i=0;
	
	this->m_name = path;
	
	n = scandir(path.c_str(), &namelist,NULL,alphasort);
	if (n < 0)
		perror("scandir");
	else {
		while (i<n) {
			chdir(path.c_str());
			lstat(namelist[i]->d_name, &statbuf);
			if(strcmp(".", namelist[i]->d_name) == 0 ||
					strcmp("..",namelist[i]->d_name) == 0 || 
					S_ISDIR(statbuf.st_mode) > 0) {
				i++;
				continue;
			}
			if (strcmp(namelist[i]->d_name + strlen(namelist[i]->d_name)-3, "mkv") == 0 ||
				strcmp(namelist[i]->d_name + strlen(namelist[i]->d_name)-3, "avi") == 0 ||
				strcmp(namelist[i]->d_name + strlen(namelist[i]->d_name)-3, "mp4") == 0 ) {
			
				this->movies.push_back(Movie(namelist[i]->d_name));
			}

			chdir("..");
			free(namelist[i]);
			++i;
		}
		free(namelist);
	}


	// ------------

/*
	DIR *dp;
	struct dirent *entry;
	struct stat statbuf;


	if((dp = opendir(path.c_str())) == NULL) {
		fprintf(stderr,"cannot open directory: EEEEEEEEEE %s\n", path.c_str());
		return;
	}
	chdir(path.c_str());
	while((entry = readdir(dp)) != NULL) {
		lstat(entry->d_name,&statbuf);
		if(S_ISDIR(statbuf.st_mode)) {
			
			if(strcmp(".",entry->d_name) == 0 ||
					strcmp("..",entry->d_name) == 0)
				continue;
		}
		else {
			if (strcmp(entry->d_name + strlen(entry->d_name)-3, "mkv") == 0 ||
				strcmp(entry->d_name + strlen(entry->d_name)-3, "avi") == 0 ||
				strcmp(entry->d_name + strlen(entry->d_name)-3, "mp4") == 0 ) {
			
				this->movies.push_back(Movie(entry->d_name));
			}
		}
	}
	chdir("..");

	closedir(dp);
*/

	this->nb_episodes = this->movies.size();
}

#include "Server.hpp"

using namespace std;

Server::Server(Vlc& vlc, vector<Movie>& movies, vector<Serie>& series) {

	this->m_movies = &movies;
	this->m_series = &series;
	this->m_vlc = &vlc;
}

void Server::start(int port) {
	
	this->m_port = port;

	this->m_server_state = true;

	int rc;
	rc = pthread_create(&server_th, NULL, thread_server, this);

	if (rc)
		cout << "Create thread error" << endl;

}

void Server::scan() {

	this->m_movies->clear();
	this->m_series->clear();

	scan_for_movies("/home/alarm/media/films", this->m_movies);
	scan_for_series("/home/alarm/media/series", this->m_series);
}

static void *thread_server(void *arg) {
       
	Server *server = (Server *)arg;

	int client_fd, server_fd;
	struct sockaddr_in server_addr;
	socklen_t size;
	char buffer[1024];
	char rep[1024];

	server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (server_fd <= 0) {
		cout << "Erreur etablishing socket" << endl;
		pthread_exit(NULL);
	}
	
	int optval = 1;
	setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, 
			(const void *)&optval , sizeof(int));

	server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htons(INADDR_ANY);
    server_addr.sin_port = htons(server->get_port());

	if ((bind(server_fd, (struct sockaddr*)&server_addr,sizeof(server_addr))) < 0) {
        cout << "=> Error binding connection, the socket has already been established..." << endl;
		pthread_exit(NULL);
    }

	size = sizeof(server_addr);

	if (listen(server_fd, 5) < 0) {
        cout << "=> Error binding connection, the socket has already been established..." << endl;
		pthread_exit(NULL);
    }

	cout << "[+] Server Started !" << endl;
	
	while (server->m_server_state) {
    	
		if ((client_fd = accept(server_fd,(struct sockaddr *)&server_addr,&size)) < 0) {
			cout << "server accept invalid socket." << endl;
			continue;
		}
		memset(buffer, 0, sizeof(buffer));
		int n =	read(client_fd, buffer, sizeof(buffer));
		cout << "client say : " << buffer << endl;

		if (strstr(buffer, "ping()") == buffer) {
			send(client_fd, "OK", 2, 0);
		}

		if (strstr(buffer, "scan()") == buffer) {
			server->scan();
			
			sprintf(rep, "{\"movies\":%d, \"series\":%d}", server->get_nb_movies(), server->get_nb_series());
			send(client_fd, rep, strlen(rep), 0);
		}

		if (strstr(buffer, "get_info_movie(") == buffer) {
			int index = atoi(buffer + strlen("get_info_movie("));

			if (index < server->get_nb_movies()) {
				string name = server->get_movie_name(index);
				send(client_fd, name.c_str(), name.size(), 0);
			}
		}
		
		if (strstr(buffer, "get_info_serie_name(") == buffer) {
			int index = atoi(buffer + strlen("get_info_serie_name("));

			if (index < server->get_nb_series()) {
				cout << "NORMAL" << endl;
				string name = server->get_serie_name(index);
				send(client_fd, name.c_str(), name.size(), 0);
			}
		}
		
		if (strstr(buffer, "get_json_serie(") == buffer) {
			int index = atoi(buffer + strlen("get_json_serie("));

			if (index < server->get_nb_series()) {
				string name = server->get_serie_json(index);
				send(client_fd, name.c_str(), name.size(), 0);
			}
		}

		if (strstr(buffer, "play(") == buffer) {
			int movie, serie, season, episode;
			int n = sscanf(buffer, "play(%d,%d,%d,%d)", &movie, &serie, &season, &episode);
			if (n == 4) {
				fprintf(stdout, "%d %d %d %d\n", movie, serie, season, episode);
				if (movie >= 0) {
					if (movie < server->get_nb_movies()) {
						server->m_vlc->play(movie, -1, -1, -1, 0, 0);
						send(client_fd, "OK", 2, 0);
					}
				}

				if (serie >= 0) {

					server->m_vlc->play(-1, serie, season, episode, 0, 0);
					send(client_fd, "OK", 2, 0);
				}
			}
		}
		
		if (strstr(buffer, "play_cmd()") == buffer) {
			server->m_vlc->play_cmd();
			send(client_fd, "OK", 2, 0);
		}

		if (strstr(buffer, "pause_cmd()") == buffer) {
			server->m_vlc->pause_cmd();
			send(client_fd, "OK", 2, 0);
		}

		if (strstr(buffer, "stop_cmd()") == buffer) {
			server->m_vlc->stop_cmd();
			send(client_fd, "OK", 2, 0);
		}
		
		if (strstr(buffer, "next_cmd()") == buffer) {
			server->m_vlc->next_cmd();
			send(client_fd, "OK", 2, 0);
		}

		close(client_fd);
	}

	pthread_exit(NULL);
}

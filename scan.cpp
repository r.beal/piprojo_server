#include "scan.h"
/*
void scan_for_series(char *dir, int depth, args_thread_t *args_thread) {
	(void)depth;

	DIR *dp;
	struct dirent *entry;
	struct stat statbuf;

	int series_counter = 0;

	if((dp = opendir(dir)) == NULL) {
		fprintf(stderr,"cannot open directory: %s\n", dir);
		return;
	}
	chdir(dir);
	while((entry = readdir(dp)) != NULL) {
		lstat(entry->d_name,&statbuf);
		if(S_ISDIR(statbuf.st_mode)) {
			if(strcmp(".",entry->d_name) == 0 ||
					strcmp("..",entry->d_name) == 0)
				continue;
			//printf("%*s%s/\n",depth,"",entry->d_name);
//			scan_for_movies(entry->d_name,depth+4, movies);
			series_counter++;
		}
	}
	chdir("..");
	closedir(dp);

	// ----------
	args_thread->nb_series = series_counter;
	args_thread->series = malloc(sizeof(series_t) *series_counter);
	// ----------

	int i=0;

	if((dp = opendir(dir)) == NULL) {
		fprintf(stderr,"cannot open directory: %s\n", dir);
		return;
	}
	chdir(dir);
	while((entry = readdir(dp)) != NULL) {
		lstat(entry->d_name,&statbuf);
		if(S_ISDIR(statbuf.st_mode)) {
			if(strcmp(".",entry->d_name) == 0 ||
					strcmp("..",entry->d_name) == 0)
				continue;
			//printf("%*s%s/\n",depth,"",entry->d_name);
//			scan_for_movies(entry->d_name,depth+4, movies);
			strncpy(args_thread->series[i].name, entry->d_name, 50);
		}
	}
	chdir("..");
	closedir(dp);

	for (i=0; i<args_thread->nb_series;i++) {
		int scanning_season = 1;
		char name_folder[100];
		int nb_season=1;
		while (scanning_season) {
			snprintf(name_folder, 5, "%s/S%d", args_thread->series[i].name, nb_season);
			if((dp = opendir(name_folder)) == NULL) {
				fprintf(stderr,"cannot open directory: %s\n", dir);
				break;
			}
			nb_season++;
		}
		fprintf(stdout, "nb_season = %d\n", nb_season);
	}

}
*/

void scan_for_series(std::string dir, vector<Serie>* series) {

	DIR *dp;
	struct dirent *entry;
	struct stat statbuf;

	if((dp = opendir(dir.c_str())) == NULL) {
		fprintf(stderr,"cannot open directory: %s\n", dir);
		return;
	}
	chdir(dir.c_str());
	while((entry = readdir(dp)) != NULL) {
		lstat(entry->d_name,&statbuf);
		if(S_ISDIR(statbuf.st_mode)) {
			
			if(strcmp(".",entry->d_name) == 0 ||
					strcmp("..",entry->d_name) == 0)
				continue;

				series->push_back(Serie(entry->d_name));
				series->back().set_name(entry->d_name);
		}
		else {
			// hihihihihii
		}

	}
	chdir("..");
	closedir(dp);
}

void scan_for_movies(std::string dir, vector<Movie>* movies) {

	DIR *dp;
	struct dirent *entry;
	struct stat statbuf;

	if((dp = opendir(dir.c_str())) == NULL) {
		fprintf(stderr,"cannot open directory: %s\n", dir);
		return;
	}
	chdir(dir.c_str());
	while((entry = readdir(dp)) != NULL) {
		lstat(entry->d_name,&statbuf);
		if(S_ISDIR(statbuf.st_mode)) {
			
			if(strcmp(".",entry->d_name) == 0 ||
					strcmp("..",entry->d_name) == 0)
				continue;
		}
		else {
			if (strcmp(entry->d_name + strlen(entry->d_name)-3, "mkv") == 0 ||
				strcmp(entry->d_name + strlen(entry->d_name)-3, "avi") == 0 ||
				strcmp(entry->d_name + strlen(entry->d_name)-3, "mp4") == 0 ) {
			
				movies->push_back(Movie(entry->d_name));
			}
		}

	}
	chdir("..");
	closedir(dp);
}

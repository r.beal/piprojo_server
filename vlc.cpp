#include "vlc.h"

using namespace std;

Vlc::Vlc(vector<Movie>& movies, vector<Serie>& series) {

	this->m_movies = &movies;
	this->m_series = &series;
	this->m_state = VLC_STATE_STOPPED;
}

Vlc::~Vlc() {

	string cmdline;
	cmdline = "killall vlc";
	system(cmdline.c_str());
}

void Vlc::play(int movie, int serie, int season, int episode, unsigned int subtrack, unsigned int audiotrack) {
	
	this->m_movie_nb = movie;
	this->m_serie_nb = serie;
	this->m_season_nb = season;
	this->m_episode_nb = episode;
	this->m_subtrack = subtrack;
	this->m_audiotrack = audiotrack;

	if (movie >= 0) {
		cout << "vlc triying to play movie : " << movie << "/" << this->m_movies->size() << endl;
		this->m_path = "media/films/";
		this->m_path.append(this->m_movies->at(movie).get_path());
	}

	if (serie >= 0) {
		this->m_path = this->m_series->at(serie).get_season(season)->get_episode(episode)->get_path();
	}

	string cmdline;
	cmdline = "killall vlc";
	system(cmdline.c_str());
// echo "su - alarm -l -c \"vlc media/films/Star\ Wars\ Episode\ V\ -\ The\ Empire\ Strikes\ Back\ \(1980\)\ MULTi\ \[1080p\]\ BluRay\ x264-PopHD.mkv\" &" | bash > /dev/tty2	
	cmdline = "echo \" su - alarm -l -c \\\"DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus cvlc --control dbus --audio-track ";
	cmdline.append(to_string(this->m_audiotrack));
    cmdline.append(" --sub-track ");
	cmdline.append(to_string(this->m_subtrack));
	cmdline.append(" \\\\\\\"");
  	cmdline.append(this->m_path);
    cmdline.append("\\\\\\\"\\\" > /dev/null 2>&1 &\" | bash > /dev/tty2");
	system(cmdline.c_str());

	this->m_state = VLC_STATE_LAUNCHING;
	this->m_start_time = time(NULL);
}

int Vlc::get_state() {

	char tmp[100];
	int pid = 0;

	FILE *fp = popen("pgrep vlc", "r");
	if (fp) {
		fgets(tmp, 100, fp);
		pid = atoi(tmp);
		pclose(fp);
	}

	if (this->m_state == VLC_STATE_LAUNCHING) {
		
		if (pid > 0)
			this->m_state = VLC_STATE_PLAY;
		else {
			if (difftime(this->m_start_time, time(NULL)) > 3) {
				this->m_state = VLC_STATE_STOPPED;
				return this->m_state;
			}
		}
	}

	if (this->m_state == VLC_STATE_PLAY && pid <= 0)
		this->m_state = VLC_STATE_FINISHED;

	if (pid > 0 && this->m_state == VLC_STATE_STOPPED)
		this->m_state = VLC_STATE_PLAY;


	cout << "RETURN " << this->m_state << " FROM GET STATE" << endl;

	return this->m_state;
}

void Vlc::play_cmd() {
	
	string cmdline;
	cmdline = "su - alarm -l -c \"DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus dbus-send --print-reply --session --dest=org.mpris.MediaPlayer2.vlc /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Play\"";
	system(cmdline.c_str());
}

void Vlc::pause_cmd() {
	
	string cmdline;
	cmdline = "su - alarm -l -c \"DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus dbus-send --print-reply --session --dest=org.mpris.MediaPlayer2.vlc /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Pause\"";
	system(cmdline.c_str());
}

void Vlc::stop_cmd() {
	
	this->m_path = "stop";

	string cmdline;
	cmdline = "killall vlc";
	system(cmdline.c_str());

	this->m_state = VLC_STATE_STOPPED;
}

void Vlc::next_cmd() {
	
	cout << "NEXT CMD !!!" << endl;

	this->m_state = VLC_STATE_FINISHED;
}

void Vlc::seek_cmd(int seek) {

}


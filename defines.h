#ifndef DEFINES_H
#define DEFINES_H

struct movie_t {
	char name[50];
	char path[100];
};
typedef struct movie_t movie_t;

struct episode_t {
	int nb;
	int season;
	char name[50];
	char path[100];
};
typedef struct episode_t episode_t;

struct series_t {

	int nb_season;
	char name[50];
	episode_t *episode;
	int nb_episode;
};
typedef struct series_t series_t;

struct args_thread_t {

	int nb_films;
	int nb_series;

	movie_t *movies;
	series_t *series;
	char *pouet;

	int player_state; // 0=OFF, 1=RUN
	int player_pid;
};
typedef struct args_thread_t args_thread_t;

#endif
